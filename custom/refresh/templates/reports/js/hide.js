//run before pageLoad
$("#navContainer").css('left','310px');
dontGoLeftSearch();


$( document ).ready(function() {
	
	$('#navOpen').click(function() {
		navOpen();
	});
	
	
	$('#navClose').click(function() {
		navClose();
	});

	//collapse navigation in mobile
	var mobileBurgerCollapse = $( window ).width();
    if(mobileBurgerCollapse < 800){
    	setTimeout(navClose, 1000);
    }
    
});


function navOpen(){
	$("#navContainer").animate({left: '310px'});
	$("#contentBG-content").css("max-width", "75%");
	$("#ppsHeaderBox").css("max-width", "400px");
	$('#navOpen').css("display", "none");
	$('#navClose').css("display", "");
	$('.contentBG').css('margin-left', '0px');	
	$('.boxGrey').css('margin-left', '0px');	
	//$("#ppsHeaderBox ").animate({right: '310px'});
	dontGoLeftSearch();

}

function navClose(){
	$("#navContainer").animate({left: '0px'});
	$("#contentBG-content").css("max-width", "100%");
	$("#ppsHeaderBox").css("max-width", "100px");
    $('#navClose').css("display", "none");
    $('#navOpen').css("display", "");
    //$("#ppsHeaderBox ").animate({right: '0px'});
	$('.contentBG').css('margin-left', '0px');
	$('.boxGrey').css('margin-left', '0px');
	$('#navContainer').css('background', '#fff');
	$('#navContainer').css('height', '100%');
    dontGoLeftSearch();
}


function dontGoLeftSearch() {
	
	var websiteWidth = $("#s4-workspace").width();
		if(websiteWidth > 500){
			var right = $("#ppsHeaderBox").css("right");	
			if(right == "0px" ){
				$("#ppsHeaderBox ").animate({right: '310px'});
			}
			else if(right == "310px" ) {
				$("#ppsHeaderBox ").animate({right: '0px'});
			}
			else {
				
			}
	}
	
}


