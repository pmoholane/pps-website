jQuery(document).ready(function(){
	adjustMain();
	var accordionsMenu = $('.cd-accordion-menu');

	if( accordionsMenu.length > 0 ) {
		
		accordionsMenu.each(function(){
			var accordion = $(this);
			//detect change in the input[type="checkbox"] value
			accordion.on('change', 'input[type="checkbox"]', function(){
				var checkbox = $(this);
				console.log(checkbox.prop('checked'));
				( checkbox.prop('checked') ) ? checkbox.siblings('ul').attr('style', 'display:none;').slideDown(300, function() {
    // Animation complete.
    adjustMain();
}) : checkbox.siblings('ul').attr('style', 'display:block;').slideUp(300, function() {
    // Animation complete.
    adjustMain();
});
			});
		});
	}
});

function adjustMain() {
	$height = $height1 = jQuery("#site-menu").height();
	$height2 = jQuery(".content-area").height();
	if($height2 > $height1) {
		$height = $height2;
		//jQuery("#site-menu").css('min-height', $height);
	}
	$("#navContainer").height($height);
}