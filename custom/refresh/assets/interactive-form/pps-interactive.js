$( document ).ready(function() {

  /*
    Allow to replay at end of each video
  */
  $("video").each(function(){

    if ($(window).width() <= 375 ) {
      var srctmp = $("source",this).attr("src");
      srctmp = srctmp.replace(/vids-desktop/gi,"vids-180p");
      $('source',this).attr("src",srctmp);
      $(this)[0].load();
    }
    else if ($(window).width() <= 667 ) {
      var srctmp = $("source",this).attr("src");
      srctmp = srctmp.replace(/vids-desktop/gi,"vids-360p");
      $('source',this).attr("src",srctmp);
      $(this)[0].load();
    }

    document.getElementById($(this).attr("id")).onended = function() {

      if (
        $(this).attr("id")!="08_Exisitng_PPS_Member_B_1" &&
        $(this).attr("id")!="08_Exisitng_PPS_Member_A_1" &&
        $(this).attr("id")!="10_Life_Sickness_and_Disability_1" &&
        $(this).attr("id")!="11_Financial_Planning_1" &&
        $(this).attr("id")!="12_Car_and_Household_Insurance_1" &&
        $(this).attr("id")!="13_Investment_Retirement_and_Savings_1" &&
        $(this).attr("id")!="14_Medical_Aid_1"
      ){
        unPauseVideo($(this).attr("id"));
      }

      //alert($(this).attr("id"));
    };
  });

  /*
    video#01_Introduction_1
  */

  $("#Content_01_Introduction_1 .btnPower .btn-danger").hide();
  $("video#01_Introduction_1 .videoContent").detach().insertAfter("video#01_Introduction_1");
  $("#Content_01_Introduction_1 .btnPower .btn-primary").on("click",function(){
    pauseVideo("01_Introduction_1");
  });
  $("#Content_01_Introduction_1 .btnPower .btn-danger").on("click",function(){
    unPauseVideo("01_Introduction_1");
  });
  //Only load next page if button clicked to go to page 2
  $("#Content_01_Introduction_1 .btnToNextStep").on("click",function(){
    loadVideo("01_Introduction_1","02_Name_1");
  });

  // On Key Enter next videos
  	$(document).bind('keypress', function(e) {
	    if(e.keyCode==13){
	    	loadVideo("01_Introduction_1","02_Name_1");
        $('html, body').animate({scrollTop:0}, 'slow');
        
	    }
	});

  /*
    video#17_Delay_06_1
  */
  /*$("video#17_Delay_06_1").hide();//Hide video by default
  var vid = document.getElementById("01_Introduction_1").onended = function() {
      loadVideo("01_Introduction_1","17_Delay_06_1");
  };*/

  /*
    video#02_Name_1
  */



  $("video#02_Name_1").hide();//Hide video by default
  $("#Content_02_Name_1 .userName,#Content_02_Name_1 .userSurname").val("");//Set fields to empty
  //Only load next page if button clicked to go to page 3
  $("#Content_02_Name_1 .btnToNextStep,#Content_03_Surname_1 .btnToNextStep").on("click",function(){
    if ($.trim($("#Content_02_Name_1 .userName, #Content_03_Surname_1 .userName").val())==""){
      $("#Content_02_Name_1 .userName, #Content_03_Surname_1 .userName").css("border","0px solid #dc3545");
      $("#Content_02_Name_1 .userSurname, #Content_03_Surname_1 .userSurname").css("border","none");
    }else if ($.trim($("#Content_02_Name_1 .userSurname, #Content_03_Surname_1 .userSurname").val())==""){
      $("#Content_02_Name_1 .userSurname, #Content_03_Surname_1 .userSurname").css("border","0px solid #dc3545");
      $("#Content_02_Name_1 .userName, #Content_03_Surname_1 .userName").css("border","none");
    }else{
      loadVideo("02_Name_1","04_Email_Address_1");
      loadVideo("03_Surname_1","04_Email_Address_1");
    }
  });


  // On Key Enter next videos
  $(document).bind('keypress', function(e) {
    if(e.keyCode==13){
        if ($.trim($("#Content_02_Name_1 .userName, #Content_03_Surname_1 .userName").val())==""){
          $("#Content_02_Name_1 .userName, #Content_03_Surname_1 .userName").css("border","0px solid #dc3545");
          $("#Content_02_Name_1 .userSurname, #Content_03_Surname_1 .userSurname").css("border","none");
        }else if ($.trim($("#Content_02_Name_1 .userSurname, #Content_03_Surname_1 .userSurname").val())==""){
          $("#Content_02_Name_1 .userSurname, #Content_03_Surname_1 .userSurname").css("border","0px solid #dc3545");
          $("#Content_02_Name_1 .userName, #Content_03_Surname_1 .userName").css("border","none");
        }else{
          loadVideo("02_Name_1","04_Email_Address_1");
          loadVideo("03_Surname_1","04_Email_Address_1");
        }
        $("input:text:visible:first").focus();
        $('html, body').animate({scrollTop:0}, 'slow');
    }
});

  /*
    video#03_Surname_1
  */
  $("video#03_Surname_1").hide();//Hide video by default
  //Only load suname video if focusing on surname
  $("#Content_02_Name_1 .userSurname").on("focus",function(event){
      $(this).off("focus");//kill event
      if ($(this).val()==""){
        //Clone content
        $("#Content_02_Name_1").detach().insertAfter("video#03_Surname_1 source");
        $("#Content_02_Name_1").attr("id","Content_03_Surname_1");
        loadVideo("02_Name_1","03_Surname_1");
        $("#Content_02_Name_1 .userSurname, #Content_03_Surname_1 .userSurname").focus();
      }
  });

  /*
    video#04_Email_Address_1
  */
  $("video#04_Email_Address_1").hide();//Hide video by default
  $("#Content_04_Email_Address_1 .emailAddress").val("");//Set fields to empty
  //Only load next page if button clicked to go to page 3
  $("#Content_04_Email_Address_1 .btnToNextStep").on("click",function(){
    if (!isEmail($.trim($("#Content_04_Email_Address_1 .emailAddress").val()))){
      $("#Content_04_Email_Address_1 .emailAddress").css("border","0px solid #dc3545");
    }else{
      loadVideo("04_Email_Address_1","05_Contact_Number_1");
    }
  });


  // On Key Enter next videos
  $(document).bind('keypress', function(e) {
	    if(e.keyCode==13){
	    	if (!isEmail($.trim($("#Content_04_Email_Address_1 .emailAddress").val()))){
		      $("#Content_04_Email_Address_1 .emailAddress").css("border","0px solid #dc3545");
		    }else{
		      loadVideo("04_Email_Address_1","05_Contact_Number_1");
		    }
		    $("input:text:visible:first").focus();
        $('html, body').animate({scrollTop:0}, 'slow');
	    }
	});

  /*
    video#05_Contact_Number_1
  */
  $("video#05_Contact_Number_1").hide();//Hide video by default
  $("#Content_05_Contact_Number_1 .contactNumber").val("");//Set fields to empty
  //Only load next page if button clicked to go to page 3
  $("#Content_05_Contact_Number_1 .btnToNextStep").on("click",function(){
    if (!$.isNumeric($.trim($("#Content_05_Contact_Number_1 .contactNumber").val())) || $.trim($("#Content_05_Contact_Number_1 .contactNumber").val()).length!=10){
      $("#Content_05_Contact_Number_1 .contactNumber").css("border","0px solid #dc3545");
    }else{
      loadVideo("05_Contact_Number_1","07_ID_Passport_Number_1");
    }
  });


  // On Key Enter next videos
  $(document).bind('keypress', function(e) {
	    if(e.keyCode==13){
	    	if (!$.isNumeric($.trim($("#Content_05_Contact_Number_1 .contactNumber").val())) || $.trim($("#Content_05_Contact_Number_1 .contactNumber").val()).length!=10){
		      $("#Content_05_Contact_Number_1 .contactNumber").css("border","0px solid #dc3545");
		    }else{
		      loadVideo("05_Contact_Number_1","07_ID_Passport_Number_1");
		    }
		    $("input:text:visible:first").focus();
        $('html, body').animate({scrollTop:0}, 'slow');
	    }
	});


  /*
    video#07_ID_Passport_Number_1
  */
  $("video#07_ID_Passport_Number_1").hide();//Hide video by default
  $("#Content_07_ID_Passport_Number_1 .IDPassportNumber").val("");//Set fields to empty
  //Only load next page if button clicked to go to page 3
  $("#Content_07_ID_Passport_Number_1 .btnToNextStep").on("click",function(){
    if ($.trim($("#Content_07_ID_Passport_Number_1 .IDPassportNumber, #Content_07_ID_Passport_Number_B_1 .IDPassportNumber").val())==""){
      $("#Content_07_ID_Passport_Number_1 .IDPassportNumber, #Content_07_ID_Passport_Number_B_1 .IDPassportNumber").css("border","0px solid #dc3545");
    }else{
      loadVideo("07_ID_Passport_Number_1","06_Highest_Qualification_1");
      loadVideo("07_ID_Passport_Number_B_1","06_Highest_Qualification_1");
    }
  });


  // On Key Enter next videos
  $(document).bind('keypress', function(e) {
	    if(e.keyCode==13){
	    	if ($.trim($("#Content_07_ID_Passport_Number_1 .IDPassportNumber, #Content_07_ID_Passport_Number_B_1 .IDPassportNumber").val())==""){
		      $("#Content_07_ID_Passport_Number_1 .IDPassportNumber, #Content_07_ID_Passport_Number_B_1 .IDPassportNumber").css("border","0px solid #dc3545");
		    }else{
		      loadVideo("07_ID_Passport_Number_1","06_Highest_Qualification_1");
		      loadVideo("07_ID_Passport_Number_B_1","06_Highest_Qualification_1");
		    }
		    $("input:text:visible:first").focus();
        $('html, body').animate({scrollTop:0}, 'slow');
	    }
	});


  /*
    video#07_ID_Passport_Number_B_1
  */
  $("video#07_ID_Passport_Number_B_1").hide();//Hide video by default
  var keyCount=0
  $(".IDPassportNumber").keyup(function(){
    keyCount++;
    if (keyCount==6){
      //Clone content
      $("#Content_07_ID_Passport_Number_1").detach().insertAfter("video#07_ID_Passport_Number_B_1 source");
      $("#Content_07_ID_Passport_Number_1").attr("id","Content_07_ID_Passport_Number_B_1")
      loadVideo("07_ID_Passport_Number_1","07_ID_Passport_Number_B_1");
      $("#Content_07_ID_Passport_Number_1 .IDPassportNumber,#Content_07_ID_Passport_Number_B_1 .IDPassportNumber").focus();
    }
  });


  /*
    video#06_Highest_Qualification_1
  */
  $("video#06_Highest_Qualification_1").hide();//Hide video by default
  $("#Content_06_Highest_Qualification_1 .heighestQualification").val("");//Set fields to empty
  //Only load next page if button clicked to go to page 3
  $("#Content_06_Highest_Qualification_1 .btnToNextStep").on("click",function(){
    if ($.trim($("#Content_06_Highest_Qualification_1 .heighestQualification").val())==""){
      $("#Content_06_Highest_Qualification_1 .heighestQualification").css("border","0px solid #dc3545");
    }else{
      loadVideo("06_Highest_Qualification_1","08_Exisitng_PPS_Member_1");
    }
  });


  // On Key Enter next videos
  $(document).bind('keypress', function(e) {
	    if(e.keyCode==13){
	    	if ($.trim($("#Content_06_Highest_Qualification_1 .heighestQualification").val())==""){
	      $("#Content_06_Highest_Qualification_1 .heighestQualification").css("border","0px solid #dc3545");
	    }else{
	      loadVideo("06_Highest_Qualification_1","08_Exisitng_PPS_Member_1");
	    }
	    $("input:text:visible:first").focus();
      $('html, body').animate({scrollTop:0}, 'slow');
	    }
	});


  /*
    video#08_Exisitng_PPS_Member_1
  */
  $("video#08_Exisitng_PPS_Member_1").hide();//Hide video by default
  $("#Content_08_Exisitng_PPS_Member_1 #existingMemberYes,#Content_08_Exisitng_PPS_Member_1 #existingMemberNo").prop("checked",false);
  //Only load next page if button clicked to go to page 3
  $("#Content_08_Exisitng_PPS_Member_1 .btnToNextStep").on("click",function(){
    if (
      !$("#Content_08_Exisitng_PPS_Member_1 #existingMemberYes, #Content_08_Exisitng_PPS_Member_A_1 #existingMemberYes, #Content_08_Exisitng_PPS_Member_B_1 #existingMemberYes").is(":checked") &&
      !$("#Content_08_Exisitng_PPS_Member_1 #existingMemberNo, #Content_08_Exisitng_PPS_Member_A_1 #existingMemberNo, #Content_08_Exisitng_PPS_Member_B_1 #existingMemberNo").is(":checked")
    ){
      $("#Content_08_Exisitng_PPS_Member_1 label, #Content_08_Exisitng_PPS_Member_A_1 label, #Content_08_Exisitng_PPS_Member_B_1 label").css("border","0px solid #dc3545");
    }
    else{
      loadVideo("08_Exisitng_PPS_Member_1","09_Im_interested_In_1");
      loadVideo("08_Exisitng_PPS_Member_A_1","09_Im_interested_In_1");
      loadVideo("08_Exisitng_PPS_Member_B_1","09_Im_interested_In_1");
    }
  });



  // On Key Enter next videos
  $(document).bind('keypress', function(e) {
	    if(e.keyCode==13){
	    	if (
		      !$("#Content_08_Exisitng_PPS_Member_1 #existingMemberYes, #Content_08_Exisitng_PPS_Member_A_1 #existingMemberYes, #Content_08_Exisitng_PPS_Member_B_1 #existingMemberYes").is(":checked") &&
		      !$("#Content_08_Exisitng_PPS_Member_1 #existingMemberNo, #Content_08_Exisitng_PPS_Member_A_1 #existingMemberNo, #Content_08_Exisitng_PPS_Member_B_1 #existingMemberNo").is(":checked")
		    ){
		      $("#Content_08_Exisitng_PPS_Member_1 label, #Content_08_Exisitng_PPS_Member_A_1 label, #Content_08_Exisitng_PPS_Member_B_1 label").css("border","0px solid #dc3545");
		    }
		    else{
		      loadVideo("08_Exisitng_PPS_Member_1","09_Im_interested_In_1");
		      loadVideo("08_Exisitng_PPS_Member_A_1","09_Im_interested_In_1");
		      loadVideo("08_Exisitng_PPS_Member_B_1","09_Im_interested_In_1");
		    }
		    $("input:text:visible:first").focus();
        $('html, body').animate({scrollTop:0}, 'slow');
	    }
	});

  /*
    video#08_Exisitng_PPS_Member_B_1
  */
  $("video#08_Exisitng_PPS_Member_B_1").hide();//Hide video by default
  $("#Content_08_Exisitng_PPS_Member_1 #existingMemberYes").on("click",function(event){
      //Clone content
      $("#Content_08_Exisitng_PPS_Member_1,#Content_08_Exisitng_PPS_Member_A_1").detach().insertAfter("video#08_Exisitng_PPS_Member_B_1 source");
      $("#Content_08_Exisitng_PPS_Member_1,#Content_08_Exisitng_PPS_Member_A_1").attr("id","Content_08_Exisitng_PPS_Member_B_1");
      loadVideo("08_Exisitng_PPS_Member_1","08_Exisitng_PPS_Member_B_1");
      loadVideo("08_Exisitng_PPS_Member_A_1","08_Exisitng_PPS_Member_B_1");
      $("#Content_08_Exisitng_PPS_Member_1 #existingMemberYes").focus();
      $("#Content_08_Exisitng_PPS_Member_A_1 .btnPower a, #Content_08_Exisitng_PPS_Member_B_1 .btnPower a").hide();//Hide pause/play button
  });

  /*
    video#08_Exisitng_PPS_Member_A_1
  */
  $("video#08_Exisitng_PPS_Member_A_1").hide();//Hide video by default
  $("#Content_08_Exisitng_PPS_Member_1 #existingMemberNo").on("click",function(event){
      //Clone content
      $("#Content_08_Exisitng_PPS_Member_1,#Content_08_Exisitng_PPS_Member_B_1").detach().insertAfter("video#08_Exisitng_PPS_Member_A_1 source");
      $("#Content_08_Exisitng_PPS_Member_1,#Content_08_Exisitng_PPS_Member_B_1").attr("id","Content_08_Exisitng_PPS_Member_A_1");
      loadVideo("08_Exisitng_PPS_Member_1","08_Exisitng_PPS_Member_A_1");
      loadVideo("08_Exisitng_PPS_Member_B_1","08_Exisitng_PPS_Member_A_1");
      $("#Content_08_Exisitng_PPS_Member_1 #existingMemberNo").focus();
      $("#Content_08_Exisitng_PPS_Member_A_1 .btnPower a, #Content_08_Exisitng_PPS_Member_B_1 .btnPower a").hide();//Hide pause/play button
  });

  /*
    video#09_Im_interested_In_1
  */
  $("video#09_Im_interested_In_1").hide();//Hide video by default
  $("#Content_09_Im_interested_In_1 #memberInterestLife,#Content_09_Im_interested_In_1 #memberInterestMedical,#Content_09_Im_interested_In_1 #memberInterestFinancial,#Content_09_Im_interested_In_1 #memberInterestInvestment,#Content_09_Im_interested_In_1 #memberInterestCarHousehold").prop("checked",false);
  //Only load next page if button clicked to go to page 3
  $("#Content_09_Im_interested_In_1 .btnToNextStep").on("click",function(){
    if (
      !$("#Content_09_Im_interested_In_1 #memberInterestLife,#Content_10_Life_Sickness_and_Disability_1 #memberInterestLife,#Content_11_Financial_Planning_1 #memberInterestLife,#Content_12_Car_and_Household_Insurance_1 #memberInterestLife,#Content_13_Investment_Retirement_and_Savings_1 #memberInterestLife,#Content_14_Medical_Aid_1 #memberInterestLife").is(":checked") &&
      !$("#Content_09_Im_interested_In_1 #memberInterestMedical,#Content_10_Life_Sickness_and_Disability_1 #memberInterestMedical,#Content_11_Financial_Planning_1 #memberInterestMedical,#Content_12_Car_and_Household_Insurance_1 #memberInterestMedical,#Content_13_Investment_Retirement_and_Savings_1 #memberInterestMedical,#Content_14_Medical_Aid_1 #memberInterestMedical").is(":checked") &&
      !$("#Content_09_Im_interested_In_1 #memberInterestFinancial,#Content_10_Life_Sickness_and_Disability_1 #memberInterestFinancial,#Content_11_Financial_Planning_1 #memberInterestFinancial,#Content_12_Car_and_Household_Insurance_1 #memberInterestFinancial,#Content_13_Investment_Retirement_and_Savings_1 #memberInterestFinancial,#Content_14_Medical_Aid_1 #memberInterestFinancial").is(":checked") &&
      !$("#Content_09_Im_interested_In_1 #memberInterestInvestment,#Content_10_Life_Sickness_and_Disability_1 #memberInterestInvestment,#Content_11_Financial_Planning_1 #memberInterestInvestment,#Content_12_Car_and_Household_Insurance_1 #memberInterestInvestment,#Content_13_Investment_Retirement_and_Savings_1 #memberInterestInvestment,#Content_14_Medical_Aid_1 #memberInterestInvestment").is(":checked") &&
      !$("#Content_09_Im_interested_In_1 #memberInterestCarHousehold,#Content_10_Life_Sickness_and_Disability_1 #memberInterestCarHousehold,#Content_11_Financial_Planning_1 #memberInterestCarHousehold,#Content_12_Car_and_Household_Insurance_1 #memberInterestCarHousehold,#Content_13_Investment_Retirement_and_Savings_1 #memberInterestCarHousehold,#Content_14_Medical_Aid_1 #memberInterestCarHousehold").is(":checked")
    ){
      $("#Content_09_Im_interested_In_1 label,#Content_10_Life_Sickness_and_Disability_1 label,#Content_11_Financial_Planning_1 label,#Content_12_Car_and_Household_Insurance_1 label,#Content_13_Investment_Retirement_and_Savings_1 label,#Content_14_Medical_Aid_1 label").css("border","0px solid #dc3545");
    }
    else{
      loadVideo("09_Im_interested_In_1","16_Submit_1");
      load09_Im_interested_In_1Videos("16_Submit_1");
    }
  });


    // On Key Enter next videos
  $(document).bind('keypress', function(e) {
	    if(e.keyCode==13){
	    	if (
		      !$("#Content_09_Im_interested_In_1 #memberInterestLife,#Content_10_Life_Sickness_and_Disability_1 #memberInterestLife,#Content_11_Financial_Planning_1 #memberInterestLife,#Content_12_Car_and_Household_Insurance_1 #memberInterestLife,#Content_13_Investment_Retirement_and_Savings_1 #memberInterestLife,#Content_14_Medical_Aid_1 #memberInterestLife").is(":checked") &&
		      !$("#Content_09_Im_interested_In_1 #memberInterestMedical,#Content_10_Life_Sickness_and_Disability_1 #memberInterestMedical,#Content_11_Financial_Planning_1 #memberInterestMedical,#Content_12_Car_and_Household_Insurance_1 #memberInterestMedical,#Content_13_Investment_Retirement_and_Savings_1 #memberInterestMedical,#Content_14_Medical_Aid_1 #memberInterestMedical").is(":checked") &&
		      !$("#Content_09_Im_interested_In_1 #memberInterestFinancial,#Content_10_Life_Sickness_and_Disability_1 #memberInterestFinancial,#Content_11_Financial_Planning_1 #memberInterestFinancial,#Content_12_Car_and_Household_Insurance_1 #memberInterestFinancial,#Content_13_Investment_Retirement_and_Savings_1 #memberInterestFinancial,#Content_14_Medical_Aid_1 #memberInterestFinancial").is(":checked") &&
		      !$("#Content_09_Im_interested_In_1 #memberInterestInvestment,#Content_10_Life_Sickness_and_Disability_1 #memberInterestInvestment,#Content_11_Financial_Planning_1 #memberInterestInvestment,#Content_12_Car_and_Household_Insurance_1 #memberInterestInvestment,#Content_13_Investment_Retirement_and_Savings_1 #memberInterestInvestment,#Content_14_Medical_Aid_1 #memberInterestInvestment").is(":checked") &&
		      !$("#Content_09_Im_interested_In_1 #memberInterestCarHousehold,#Content_10_Life_Sickness_and_Disability_1 #memberInterestCarHousehold,#Content_11_Financial_Planning_1 #memberInterestCarHousehold,#Content_12_Car_and_Household_Insurance_1 #memberInterestCarHousehold,#Content_13_Investment_Retirement_and_Savings_1 #memberInterestCarHousehold,#Content_14_Medical_Aid_1 #memberInterestCarHousehold").is(":checked")
		    ){
		      $("#Content_09_Im_interested_In_1 label,#Content_10_Life_Sickness_and_Disability_1 label,#Content_11_Financial_Planning_1 label,#Content_12_Car_and_Household_Insurance_1 label,#Content_13_Investment_Retirement_and_Savings_1 label,#Content_14_Medical_Aid_1 label").css("border","0px solid #dc3545");
		    }
		    else{
		      loadVideo("09_Im_interested_In_1","16_Submit_1");
		      load09_Im_interested_In_1Videos("16_Submit_1");
		    }
		    $("input:text:visible:first").focus();
        $('html, body').animate({scrollTop:0}, 'slow');
	    }
	});


  /*
    video#10_Life_Sickness_and_Disability_1
  */
  $("video#10_Life_Sickness_and_Disability_1").hide();//Hide video by default
  switch09_Im_interested_In_1Videos("10_Life_Sickness_and_Disability_1","memberInterestLife");

  /*
    video#11_Financial_Planning_1
  */
  $("video#11_Financial_Planning_1").hide();//Hide video by default
  switch09_Im_interested_In_1Videos("11_Financial_Planning_1","memberInterestFinancial");

  /*
    video#12_Car_and_Household_Insurance_1
  */
  $("video#12_Car_and_Household_Insurance_1").hide();//Hide video by default
  switch09_Im_interested_In_1Videos("12_Car_and_Household_Insurance_1","memberInterestCarHousehold");
  /*
    video#13_Investment_Retirement_and_Savings_1
  */
  $("video#13_Investment_Retirement_and_Savings_1").hide();//Hide video by default
  switch09_Im_interested_In_1Videos("13_Investment_Retirement_and_Savings_1","memberInterestInvestment");

  /*
    video#14_Medical_Aid_1
  */
  $("video#14_Medical_Aid_1").hide();//Hide video by default
  switch09_Im_interested_In_1Videos("14_Medical_Aid_1","memberInterestMedical");

  function load09_Im_interested_In_1Videos(selectedVideo){
    loadVideo("09_Im_interested_In_1",selectedVideo);
    loadVideo("10_Life_Sickness_and_Disability_1",selectedVideo);
    loadVideo("11_Financial_Planning_1",selectedVideo);
    loadVideo("12_Car_and_Household_Insurance_1",selectedVideo);
    loadVideo("13_Investment_Retirement_and_Savings_1",selectedVideo);
    loadVideo("14_Medical_Aid_1",selectedVideo);
  }

  function switch09_Im_interested_In_1Videos(selectedVideo, memberInterest){
    $("#Content_09_Im_interested_In_1 #"+memberInterest).on("click",function(event){
      if ($(this).is(':checked')) {
        //Clone content
        $("#Content_09_Im_interested_In_1,#Content_10_Life_Sickness_and_Disability_1,#Content_11_Financial_Planning_1,#Content_12_Car_and_Household_Insurance_1,#Content_13_Investment_Retirement_and_Savings_1,#Content_14_Medical_Aid_1").detach().insertAfter("video#"+selectedVideo+" source");
        $("#Content_09_Im_interested_In_1,#Content_10_Life_Sickness_and_Disability_1,#Content_11_Financial_Planning_1,#Content_12_Car_and_Household_Insurance_1,#Content_13_Investment_Retirement_and_Savings_1,#Content_14_Medical_Aid_1").attr("id","Content_"+selectedVideo);
        load09_Im_interested_In_1Videos(selectedVideo);
        $("#Content_09_Im_interested_In_1 #memberInterestLife").focus();
        $("#Content_09_Im_interested_In_1 .btnPower a,#Content_10_Life_Sickness_and_Disability_1 .btnPower a,#Content_11_Financial_Planning_1 .btnPower a,#Content_12_Car_and_Household_Insurance_1 .btnPower a,#Content_13_Investment_Retirement_and_Savings_1 .btnPower a,#Content_14_Medical_Aid_1 .btnPower a").hide();//Hide pause/play button
      }
    });
  }

  /*
    video#16_Submit_1
  */
  $("video#16_Submit_1").hide();//Hide video by default

  function loadVideo(removeVideo,loadVideo){

      $("video#"+loadVideo+" .videoContent").detach().insertAfter("video#"+loadVideo);

      $("#Content_"+loadVideo+" .btnPower .btn-primary").on("click",function(){
        pauseVideo(loadVideo);
      });
      $("#Content_"+loadVideo+" .btnPower .btn-danger").on("click",function(){
        unPauseVideo(loadVideo);
      });

      $("video#"+removeVideo).trigger("pause");
      var mediaElement = document.getElementById(removeVideo);
      mediaElement.currentTime = 0;
      $("video#"+removeVideo).hide();
      $("#Content_"+removeVideo).hide();

      $("video#"+loadVideo).trigger("play").show();
      $("#Content_"+loadVideo).show();
      $("#Content_"+loadVideo+" .btnPower .btn-primary").hide();
      $("#Content_"+loadVideo+" .btnPower .btn-danger").show();
  }

  function pauseVideo(videoID){
     $("video#"+videoID).trigger("play");
     $("#Content_"+videoID+" .btnPower .btn-primary").hide();
     $("#Content_"+videoID+" .btnPower .btn-danger").show();
  }

  function unPauseVideo(videoID){
     $("video#"+videoID).trigger("pause");
     $("#Content_"+videoID+" .btnPower .btn-danger").hide();
     $("#Content_"+videoID+" .btnPower .btn-primary").show();
  }

  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

});
