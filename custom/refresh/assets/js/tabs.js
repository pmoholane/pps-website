﻿$(document).ready(function(){
	
	//Page tabs	
	jQuery('.tabs .tab-links a').on('click', function(e)  {
		e.preventDefault();
		currentAttrValue = jQuery(this).attr('href');
		
		// Show/Hide Tabs
		jQuery('.tabs ' + currentAttrValue).show().siblings().hide(0, function() {
			// Animation complete.
			adjustMain();
		});
				
		// Change/remove current tab to active
		jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
		
		
	});
	
	
	//Display the add accordion buttons when in edit mode
	/*
	var inDesignMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;
	
	if (inDesignMode == "1")
	{
	    document.getElementById("ppsPagelayoutControl").style.display = "";
	}
	else
	{
	   document.getElementById("ppsPagelayoutControl").style.display = "none";
	}
	*/



	//expands & colapse the tabs in mobile view
	$('#mobileButton').click(function(){
		if ($('#pageTabsContainer').is(':visible')) {
			$('#tabMobileSelector').css({'background-color': '#fff', 'color' : '#072b4d'});
			$('#mobileButton').css('background-image', 'url("/Style Library/PPS/images/mobileArrowBleu.svg")');
			$('#pageTabsContainer').slideUp("fast");
		} 
		else {
			$('#tabMobileSelector').css({'background-color': '#072b4d', 'color' : '#fff' });
			$('#mobileButton').css('background-image', 'url("/Style Library/PPS/images/mobileArrowWhite.svg")');
			$('#pageTabsContainer').slideDown("fast");
		}
	
	});

}); 