$(document).ready(function() {
	//The Credit Card Benefit download link
	$('#section-apply .card-and-payments-banner').removeAttr('style');
	$('.field--name-field-credt-benefit-dwnload-link .file--mime-application-pdf').attr('id','idSpan');	
	$('.field--name-field-credt-benefit-dwnload-link .file--mime-application-pdf').removeAttr('class');
	$("#idSpan").wrap("<p id='idLink' class='u-text-center'>");
	$("#idSpan").contents().unwrap();	
	$("#idLink a").attr("class","c-button-1 -color-green-outline -hover-green-default -size-large -corner-circle").attr("target","_blank");
    $("#idLink a").append("<i class='fas fa-chevron-right' style='margin-left: 5px;'></i>");	
	
	//The Merchant solution Terminal Benefits download link
	$(".field--name-field-terminal-download-button span").removeAttr("class");
	$('.field--name-field-terminal-download-button span').attr('id','idTerminalSpan');
	$('#idTerminalSpan').wrap('<p class="u-text-center" id="idTerminalLink">');
	$('#idTerminalSpan').contents().unwrap();
	$(".field--name-field-terminal-download-button a").attr("class","c-button-1 -color-green-outline -hover-green-default -size-large -corner-circle").attr("target","_blank");
	$("#idTerminalLink a").append("<i class='fas fa-chevron-right' style='margin-left: 5px;'></i>");
	
	
	//The Merchant solution Online Merchant Link
	$(".field--name-field-online-transaction-link a").attr('id','idTransactionLink');
	$(".field--name-field-online-transaction-link").replaceWith(function() {
		return $("a", this);
	});
	$("#idTransactionLink").wrap("<p class='u-margin-top-40 u-text-center'>").attr("class","c-button-1 -color-green-outline -hover-green-default -size-large -corner-circle").attr("target","_blank");
	$("#idTransactionLink").append("<i class='fas fa-chevron-right' style='margin-left: 5px;'></i>");
	
	$vari = $(".categoryTerm").html();
	$(".categoryTerm").each(function(index,elem) { 
		if(index != 0) { 
			$current = elem.innerHTML; 
			if($current == $vari) { 
				$(this).remove(); 
				//$(this).css("display","none");
			} 
			else 
			{ 
				$vari = $current; 
			} 
		}  
	});
	
	$(".categoryTerm:first").unwrap();
	$("#accordion").wrap("<div class='container'></div>");
	$(".field__label").remove();
	
	$(".field--name-field-internet-banking-link a").attr("class","intLink c-button-1 -color-green-outline -hover-green-default -size-large -corner-circle").attr("target","_blank");
	$(".intLink").unwrap().unwrap();
	$(".intLink").wrap("<p class='u-margin-top-40 u-margin-bottom-40 u-text-center'></p>");
	$(".intLink").append("<i class='fas fa-chevron-right' style='margin-left: 5px;'></i>");
	
	$(".field--name-field-internetbanking-app-link a").attr("class","intAppLink c-button-1 -color-green-outline -hover-green-default -size-large -corner-circle").attr("target","_blank");
	$(".intAppLink").unwrap().unwrap();
	$(".intAppLink").wrap("<p id='takeRight' class='u-text-lead'></p>");
	$(".intAppLink").append("<i class='fas fa-chevron-right' style='margin-left: 5px;'></i>");	
	
	$(".field--name-field-internet-pricing-guide a").attr("class","intPricingLink c-button-1 -color-green-outline -hover-green-default -size-large -corner-circle").attr("target","_blank");
	$(".intPricingLink").unwrap().unwrap().unwrap();
	$(".intPricingLink").wrap("<p id='takeLeft'></p>");	
	$(".intPricingLink").append("<i class='fas fa-chevron-right' style='margin-left: 5px;'></i>");	

	var myUrl = window.location.href.slice(window.location.href.lastIndexOf('/') + 1);
	if(myUrl == "credit-card") {
		$("#section-qualify .space-padding").removeClass("margin-payments");
	} 
	if(myUrl == "payment-solutions") { 
		$("#section-qualify .space-padding").addClass("margin-credit");
	}
	if(myUrl == "pps-ra-project") { 
		$("#section-qualify .space-padding").addClass("margin-credit");
	}

	var ourUrl = window.location.href.slice(window.location.href.lastIndexOf('/') + 1);	
	
	if(ourUrl == "credit-card") { 
		var dta = ""; 
		$.ajax({
				method: "GET",
				url: "/faqs/credit-card"
			})
			.done(function( msg ) {
				//dta = $('.block-views-blockfaq-template-faq-payment-solutions-block').children(".card"); 
				//dta = $(msg).find("#accordion"); 
				dta=$(msg).find("#accordion>div>div>div");
				//dta = $(dta).closest(".credit:first").remove();
				//dta = $(dta).find(".credit:first").unwrap();
				$("#accordion").append(dta);
		});  
	}

	var ppsra = window.location.href.slice(window.location.href.lastIndexOf('/') + 1);	

	if(ppsra == "ppsra") { 
	  console.log(ppsra);
		var dta = ""; 
		$.ajax({
				method: "GET",
				url: "/faqs/faq-ra-project"
			})
			.done(function( msg ) {
				//dta = $('.block-views-blockfaq-template-faq-payment-solutions-block').children(); 
				//dta = $(msg).find("#accordion"); 
				dta=$(msg).find("#accordion>div>div");
				//dta = $(dta).find(".view-grouping-header").remove();
				//dta = $(dta).find(".payment:first").unwrap();
				$("#accordion").append(dta);
				console.log('|?|?|??|||?|?|');
				console.log(dta);
				console.log('|?|?|??|||?|?|');
				$(".payment-solutions-form").removeAttr('hidden');
				$(".paypent-solutions-modal").removeAttr('hidden');
				$('.control-padding').removeClass('col-md-3'); $('.control-padding').addClass('col-md-4');
				
				
		});  
	}

	//$(".view-grouping-header").remove();
	//$(".credit:first").unwrap().unwrap();	
	if(ourUrl == "payment-solutions") { 
	  
		var dta = ""; 
		$.ajax({
				method: "GET",
				url: "/faqs/payment-solutions"
			})
			.done(function( msg ) {
				//dta = $('.block-views-blockfaq-template-faq-payment-solutions-block').children(); 
				//dta = $(msg).find("#accordion"); 
				dta=$(msg).find("#accordion>div>div");
				//dta = $(dta).find(".view-grouping-header").remove();
				//dta = $(dta).find(".payment:first").unwrap();
				$("#accordion").append(dta);
				$(".payment-solutions-form").removeAttr('hidden');
				$(".paypent-solutions-modal").removeAttr('hidden');
				$('.control-padding').removeClass('col-md-3'); $('.control-padding').addClass('col-md-4');
				
				
		});  
	}
	
	var aUrl = window.location.href.slice(window.location.href.lastIndexOf('/') + 1);

	if(aUrl == "contact-pps") 
	{
		var dta = "";
		$.ajax({ 
			method: "GET", 
			url: "/contactus" 
		}).done(function(msg) {   
			dta = $(msg).find("#accordion>div>").appendTo("#accordion");	
			console.log("<><><><><><");	
			console.log(dta);
			console.log("<><><><><><");	
		});
	}

	//$(".view-grouping-header").remove();
	//$(".payment:first").unwrap().unwrap();
	
	//Search functionality in the FAQs
	$("#idSubmit").on("click",function(){
		if(filter != "") {
			var input, filter, results, txtValue;
			input = document.getElementById("myInput");
			filter = input.value.toUpperCase();

			result = document.getElementsByClassName("search");

			$(result).each(function(i,elem) {
				txtValue = result[i].innerHTML || result[i].textContent;
				if(txtValue.toUpperCase().indexOf(filter) > -1){
					$(this).parent().parent().addClass('found');
						
					$(this).parent().parent().prevAll(".categoryTerm").addClass("found");
				} 
				else {
					if($(this).parent().parent().prevAll(".categoryTerm").hasClass("found") == false) {
						$(this).parent().parent().prevAll(".categoryTerm").addClass("NOTfound");
					}
				}
			});

			$(".card").each(function(index, elem) {
				if($(this).hasClass("found") == false) {
					$(this).hide();
				}
			});
			
			$(".categoryTerm").each(function(index, elem) {
				if($(this).hasClass("found") == false) {
					$(this).hide();
				}
			});			
			
			$(".card").removeClass("found");
		}
	});

	//Seaarch for the contact us page
	$("#idContactSubmit").on("click",function(){
		if(filter != "") {
			var input, filter, results, txtValue;
			input = document.getElementById("myContactInput");
			filter = input.value.toUpperCase();

			result = document.getElementsByClassName("search");

			$(result).each(function(i,elem) {
				txtValue = result[i].innerHTML || result[i].textContent;
				if(txtValue.toUpperCase().indexOf(filter) > -1){
					$(this).parent().parent().addClass('found');

				} 
			});

			$(".card").each(function(index, elem) {
				if($(this).hasClass("found") == false) {
					$(this).hide();
				}
			});
			
			$(".card").removeClass("found");
		}
	});	

	$(".field--name-field-payment-solutions-image img").attr("id","payment-solutions-image");
	$("#payment-solutions-image").unwrap();
	$("#payment-solutions-image").removeAttr("height").removeAttr("width").css("width","100%").css("height","480px");	
	
	var urlMy = window.location.href.slice(window.location.href.lastIndexOf('/') + 1);
	if(urlMy == "credit-card" || urlMy == "payment-solutions") {
		$("#accordion").parent().attr("id","container-id");
	}
	
	var urlFAQ = window.location.href.slice(window.location.href.lastIndexOf('/') + 1);
	
	//NOTHING FINAL YET
	/*if(urlFAQ == "all") {
		$varia = $(".categoryTerm").html();
		$arrayH3 = [];
		$(".categoryTerm").each(function(index, elem){
			console.log($(this).attr("id"));
			$arrayH3.push($(this).attr("id"));
		});
	}*/
	
	$(".field--name-field-intro-link a").addClass("btn btn-blue btn-rounded px-3");
	var alternative = [];
	$(".element-center img").each(function() {
		alternative.push($(this).attr('alt')); 	
	});

	for(var i = 0; i < alternative.length; i++) {
		$(".element-center p").each(function() {
			if($(this).attr('id') == 'p' + i) {
				$(this).text(alternative[i]);
				//console.log(alternative[i]);
			}
		});
	}

	$(".field--name-field-university-support-file a").addClass("aLink c-button-1 -color-green-outline -hover-green-default -size-large -corner-circle").attr("target","_blank");
	$(".field--name-field-university-support-file a").unwrap();
	$(".field--name-field-intro-link a").addClass("btn btn-blue btn-rounded px-3");
	$(".test:even").css("background-color","#f6f6f6"); 
	$(".test:odd").css("background-color","#ffffff"); 
	$(".field--name-field-wealth-banner-link a").addClass("c-button-1 -color-green-outline -hover-green-default -size-large -corner-circle");	

	var testimo = window.location.href.slice(window.location.href.lastIndexOf('/') + 1);

	if(testimo == "testimonials"){
		var id = localStorage.getItem("anchorID");
		localStorage.clear();

		var link = document.createElement('a');
		link.href = "#"+id;
		document.body.appendChild(link);
		link.click();
		document.getElementById(id).style.marginTop = "109px !important";
		//$("#"+id).css("margin-top","109px");
	}
	$(".field--name-field-mutuality-link a").attr("class","font-weight-bold btn btn-transparent-blue btn-rounded");
	var widt = $(window).width();
	if(widt <= 500) {
		$(".contact-area").removeAttr("style");
		$(".contact-area .boxed").removeAttr("data-aos").removeAttr("data-aos-anchor-placement").removeClass("aos-init").removeClass("aos-animate");
	}
	$("#homeBannerLink a").addClass("btn btn-gold btn-rounded px-3");	
	$("#hpi_link a").addClass("btn btn-blue btn-rounded px-3");
});

	function phalaza(elem) {
		
			event.stopPropagation();
			var id = $(elem).attr("aria-controls");
			
			if($(elem).hasClass("collapsed") == true) {
				$(".link-list").each(function(i,val) {
					
					var ids = $(this).attr("aria-controls");
					$(this).addClass("collapsed");
					$(this).attr("aria-expanded","true");
					$("#"+ids).removeClass("show");	
				});
					$(elem).removeClass("collapsed");
					$(elem).attr("aria-expanded","false");
					$("#"+id).addClass("show");			
			} else {
				$(elem).addClass("collapsed");	
				$(elem).attr("aria-expanded","true");	
				$("#"+id).removeClass("show");				
			}
	}

	function toTestimonials(id) {
		localStorage.setItem("anchorID",id);
		location.replace("/testimonials");
	}