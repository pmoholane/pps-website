/*Start of Contact Form Integration */
function submitContactForm(){
	var title = $("#title").find(":selected").text();		
	var firstname = $("#firstname").val();	
	var lastname = $("#lastname").val();
	var email = $("#email").val();
	var contact = $("#contact").val();
	var passport = $("#passport").val();
	var city = $("#city").val();
	var information_type = $("#information_type").find(":selected").text();
	var contact_time = $("#contact_time").find(":selected").text();	
	var referringUrl = document.referrer;
	
	var message_type = [];

	if($("#life_cover").prop("checked") == true){		
		message_type.push($("#life_cover").val());
	}
	if($("#financial_planning").prop("checked") == true){
		message_type.push($("#financial_planning").val());
	}

	if($("#medical_aid").prop("checked") == true){		
		message_type.push($("#medical_aid").val());
	}
	
	if($("#car_household").prop("checked") == true){	
		message_type.push($("#car_household").val());
	}

	if($("#invest_save").prop("checked") == true){		
		message_type.push($("#invest_save").val());
	}

	if($("#business_insurance").prop("checked") == true){	
		message_type.push($("#business_insurance").val());
	}
	
	if($("#credit_payment").prop("checked") == true){	
		message_type.push($("#credit_payment").val());
	}
	
	if($("#not_sure").prop("checked") == true){			
		message_type.push($("#not_sure").val());	
	}	
	
	var message = $("#message").val();
	

	if ($.trim(title) && $.trim(firstname) && $.trim(lastname) && $.trim(email) && $.trim(contact) && $.trim(passport) && $.trim(information_type) && $.trim(contact_time) && $.trim(city) ){
			
		ContactFunction(referringUrl, title,firstname, lastname, email, contact, passport, city, information_type, contact_time, message_type, message);
	
	}else{
		//console.log("form will not be sent");
	}
	  
}

function ContactFunction(referringUrl, title,firstname, lastname, email, contact, passport, city, information_type, contact_time, message_type, message){	
	$.ajax({
		cache: false,
		contentType:'application/json',
		method: 'POST',
		/*url: 'http://integrations.pps.co.za/api/v1/contactUs',*/
		url: 'http://stgintegrations.pps.co.za/api/v1/contactUs',
		data: JSON.stringify({
			"referringUrl":referringUrl,
			"telephoneNumber":contact,
			"queryRelations":message_type,
			"title":title,
			"message":message,
			"city": city,
			"firstName":firstname,
			"submissionType":information_type,
			"emailAddress":email,
			"surname":lastname,
			"preferedContactTime":contact_time,
			"idNumber":passport
		}),
		error: function (data) {
			//console.log(data);
			
		},
		success: function (data) {
			//console.log(data); 
			window.location = "/thank-you";
		
		},
		complete: function(data){
			//console.log(data);
		}
	});
	
}

$("#submitContact").on("click",function() {
	submitContactForm();
});

$("#submitComplaints").on("click",function() {
	submitContactForm();
});

/*End of Contact Form Integration */

/*Start of Join PPS Form Integration */
function submitJoinPPSForm(){
	var title = $("#title").find(":selected").text();		
	var firstname = $("#firstname").val();	
	var lastname = $("#lastname").val();
	var email = $("#email").val();
	var contact = $("#contact").val();
	var passport = $("#passport").val();
	var city = $("#city").val();
	var contact_time = $("#contact_time").find(":selected").text();	
	var referringUrl = document.referrer;	
	var qualification = $("#qualification").val();			
	var occupation = $("#occupation").val();
	
	var message_type = [];

	if($("#life_cover").prop("checked") == true){		
		message_type.push($("#life_cover").val());
	}
	if($("#financial_planning").prop("checked") == true){
		message_type.push($("#financial_planning").val());
	}

	if($("#medical_aid").prop("checked") == true){		
		message_type.push($("#medical_aid").val());
	}
	
	if($("#car_household").prop("checked") == true){	
		message_type.push($("#car_household").val());
	}

	if($("#invest_save").prop("checked") == true){		
		message_type.push($("#invest_save").val());
	}

	if($("#business_insurance").prop("checked") == true){	
		message_type.push($("#business_insurance").val());
	}
	
	if($("#credit_payment").prop("checked") == true){	
		message_type.push($("#credit_payment").val());
	}
	
	if($("#not_sure").prop("checked") == true){			
		message_type.push($("#not_sure").val());	
	}	
	

	if ($.trim(title) && $.trim(firstname) && $.trim(lastname) && $.trim(email) && $.trim(contact) && $.trim(passport) && $.trim(qualification) && $.trim(occupation) && $.trim(city) && $.trim(contact_time) ){
			
		JoinPPSFunction(referringUrl, title,firstname, lastname, email, contact, passport, city, qualification, contact_time, occupation, message_type);
	
	}else{
		//console.log("form will not be sent");
	}
	  
}

function JoinPPSFunction(referringUrl, title,firstname, lastname, email, contact, passport, city, qualification, contact_time, occupation, message_type) 
{
	$.ajax({
		cache: false,
		contentType:'application/json',
		method: 'POST',
		url: 'http://integrations.pps.co.za/api/v1/joinPPSAdvice',
		data: JSON.stringify({
		  "occupation": occupation,
		  "highestQualification": qualification,
		  "title": title,
		  "referrinfUrl": referringUrl,
		  "city": city,
		  "emailAddress": email,
		  "preferredContactTime": contact_time,
		  "surname": lastname,
		  "name": firstname,
		  "contactNumber": contact,
		  "idNumber": passport,
		  "queryRelations": message_type
		}),
		error: function (data) {
			//console.log(data);
			
		},
		success: function (data) {
			//console.log(data); 
			window.location = "/thank-you";
		
		},
		complete: function(data){
			//console.log(data);
		}
	});
	
}

$("#submitJoinPPS").on("click",function() {
	submitJoinPPSForm();
});
/*End of Contact Form Integration */

/*Start of Horizon Adviser Form Integration */
function submitHorizonAdviserForm(){
	var accreditation = $("#accreditation").find(":selected").text();		
	var code = $("#code").val();	
	var broker_name = $("#broker_name").val();
	var contact_name = $("#contact_name").val();
	var email = $("#email").val();
	var contact = $("#contact").val();
	var city = $("#city").val();
	var referringUrl = document.referringUrl;


	if ($.trim(accreditation) && $.trim(code) && $.trim(broker_name) && $.trim(contact_name) && $.trim(email) && $.trim(contact) && $.trim(city) ){
			
		HorizonAdviserFunction(referringUrl, accreditation,code, broker_name, contact_name, email, contact, city);
	
	}else{
		//console.log("form will not be sent");
	}
	  
}

function HorizonAdviserFunction(referringUrl, accreditation,code, broker_name, contact_name, email, contact, city) 
{
	
	$.ajax({
		cache: false,
		contentType:'application/json',
		method: 'POST',
		url: 'http://integrations.pps.co.za/api/v1/horizonAdviser',
		data: JSON.stringify({
		  "serialVersionUID": 0,
		  "emailAddress": email,
		  "city": city,
		  "ppsInvestmentAccredited": accreditation,
		  "contactName": contact_name,
		  "brokerageCode": code,
		  "brokerageName": broker_name,
		  "contactNumber": contact,
		  "referringUrl": referringUrl
		}),
		error: function (data) {
			//console.log(data);
			
		},
		success: function (data) {
			//console.log(data); 
			window.location = "/thank-you";
		
		},
		complete: function(data){
			//console.log(data);
		}
	});
	
}

$("#submitHorizonAdviser").on("click",function() {
	submitHorizonAdviserForm();
});
/*End of Contact Form Integration */
/*Start of Financial Plan Form Integration */
function submitfinancialPlanForm(){		
	var fullname = $("#fullname").val();	
	var contactnumber = $("#contactnumber").val();
	var emailaddress = $("#emailaddress").val();
	var dayOfWeek = $("#dayOfWeek").find(":selected").text();
	var timeOfWeek = $("#timeOfWeek").find(":selected").text();	
	var referringUrl = document.referringUrl;


	if ($.trim(fullname) && $.trim(contactnumber) && $.trim(emailaddress) && $.trim(dayOfWeek) && $.trim(timeOfWeek)){
			
		FinancialPlanFunction(referringUrl, fullname, contactnumber, emailaddress, dayOfWeek, timeOfWeek);
	
	}else{
		//console.log("form will not be sent");
	}
	  
}

function FinancialPlanFunction(referringUrl, fullname, contactnumber, emailaddress, dayOfWeek, timeOfWeek)
{
	$.ajax({
		cache: false,
		contentType:'application/json',
		method: 'POST',
		url: 'http://integrations.pps.co.za/api/v1/financialAdviser',
		data: JSON.stringify({
		  "dayToContact": dayOfWeek,
		  "emailAddress": emailaddress,
		  "preferredContactTime": timeOfWeek,
		  "contactNumber": contactnumber,
		  "fullName": fullname,
		  "referringUrl":referringUrl
		}),
		error: function (data) {
			//console.log(data);
			
		},
		success: function (data) {
			//console.log(data); 
			window.location = "/thank-you";
		
		},
		complete: function(data){
			//console.log(data);
		}
	});
	
}

$("#submitFinancialPlan").on("click",function() {
	submitfinancialPlanForm();
});
/*End of Contact Form Integration */
/*Start of Financial Plan Form Integration */
function submitInvestForm(){		
	var title = $("#title").find(":selected").text();	
	var firstname = $("#firstname").val();
	var lastname = $("#lastname").val();
	var email = $("#email").val();
	var contact = $("#contact").val();	
	
	var city = $("#city").val();
	var contact_time = $("#contact_time").find(":selected").text();
	var qualification = $("#qualification").val();
	var occupation = $("#occupation").val();	
	var referringUrl = document.referringUrl;
	

	if ($.trim(title) && $.trim(firstname) && $.trim(lastname) && $.trim(email) && $.trim(contact) && $.trim(city) && $.trim(contact_time) && $.trim(qualification) && $.trim(occupation)){
			
		InvestFunction(referringUrl, title, firstname, lastname, email, contact, city, contact_time, qualification, occupation);
	
	}else{
		//console.log("form will not be sent");
	}
	  
}

function InvestFunction(referringUrl, title, firstname, lastname, email, contact, city, contact_time, qualification, occupation)
{
	$.ajax({
		cache: false,
		contentType:'application/json',
		method: 'POST',
		url: 'http://integrations.pps.co.za/api/v1/ppsiContactUs',
		data: JSON.stringify({
		  "city": city,
		  "emailAddress": email,
		  "occupation": occupation,
		  "preferredContactTime": contact_time,
		  "highestQualification": qualification,
		  "surname": lastname,
		  "name": firstname,
		  "contactNumber": contact,
		  "title": title,
		  "referringUrl": referringUrl
		}),
		error: function (data) {
			//console.log(data);
		},
		success: function (data) {
			//console.log(data); 
			window.location = "/thank-you";	
		},
		complete: function(data){
			//console.log(data);
		}
	});
	
}

$("#submitPPSIContactUs").on("click",function() {
	submitInvestForm();
});
/*End of Contact Form Integration */