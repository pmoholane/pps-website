'use strict';
var PPSWebChat = window.PPSWebChat || {};

PPSWebChat.Functions = function(){
	
	var ServiceURL = "";
	
	var GlobalParticipantID = "";
	
	//Function to set ServiceURL property.
	var SetServiceURL = function(URL){
		ServiceURL = URL;
	};
	
	//Function to set GlobalParticipantID property.
	var SetGlobalParticipantID = function(ParticipantID){
		GlobalParticipantID = ParticipantID;
	};
	
	//Polling function to check for new Messages, if new message is found then Chat Window and Session Cache is updated.
	var pollForNewMessages = function (ParticipantID){
		
		try
		{
			$.ajax({
			  type: "GET",
			  dataType: "json",
			  url: ServiceURL + "/WebSvcs/chat/poll/" + ParticipantID,
				success: function( data, textStatus, jQxhr ){
					var NewValue = "";
					
					if(data.chat.events)
					{
						for(var x = 0; x < data.chat.events.length; x++)
						{
							if(data.chat.events[x].value && data.chat.events[x].type == "text")
							{
								var DisplayName = "";
								var participantType = ""
									if(data.chat.events[x].displayName)
									{
										DisplayName = data.chat.events[x].displayName;
									}
									
									if(data.chat.events[x].participantType)
									{
										participantType = data.chat.events[x].participantType;
									}
									
									
										
									if(participantType == "System")
									{
										NewValue =  "<div class='PPSWebChat-Server'>" + NewValue + "" + DisplayName + ": " + data.chat.events[x].value + "</div>";
									}
									else if(participantType == "Agent")
									{
										NewValue =  "<div class='PPSWebChat-Agent'>" + NewValue + "" + DisplayName + ": " + data.chat.events[x].value + "</div>";

									}
									else
									{
										NewValue =  "<div class='PPSWebChat-Client'>" + NewValue + "" + DisplayName + ": " + data.chat.events[x].value + "</div>";
									}
									
							}
							if(data.chat.events[x].type == "typingIndicator")
							{
								if(data.chat.events[x].value == true)
								{
									$("#PPSWebChat_AgentStatus").html("<p>Typing...</p>");
								}
								else
								{
									$("#PPSWebChat_AgentStatus").html("");
								}

							}
							if(data.chat.events[x].type == "participantStateChanged")
							{
							
									if(data.chat.events[x].participantType == "System")
									{

									}
									else if(data.chat.events[x].participantType == "Agent")
									{
										StoreToSession("PPSWebChat_AgentName",data.chat.events[x].participantName);
										$("#PPSWebChat_AgentName").html(data.chat.events[x].participantName);
									}
									else
									{
										StoreToSession("PPSWebChat_ClientName",$( "#PPSWebChat_Name" ).val());
										$("#PPSWebChat_ClientName").html($( "#PPSWebChat_Name" ).val());
									}

							}
						
							//console.log(data.chat.events[x]);

						}
					}
					
					if(NewValue != "")
					{
						StoreToSession('PPSWebChat_ChatHistory',PPSWebChat.Functions.GetFromSession("PPSWebChat_ChatHistory") + NewValue);
								
						$("#PPSWebChat_ChatHistory").append(NewValue);				
						$('#PPSWebChat_ChatHistory').scrollTop($('#PPSWebChat_ChatHistory').prop("scrollHeight"));					
					}

					
					setTimeout(pollForNewMessages(ParticipantID),1000);
					
				},
				error: function( jqXhr, textStatus, errorThrown ){
					//alert(errorThrown);
					console.log( errorThrown );
				}
			});
		}	
		catch(err)
		{
			alert(err);
			console.log(err);
		}
		

		
	};

	//Function to send a message to the Service when the User Clicks the Send button.
	var sendWebChatMessage = function (){
		
		try
		{
			var data = {
			"message": $( "#PPSWebChat_Message" ).val(),
			"contentType": "text/plain"
			};
	
			$.ajax({
			  type: "POST",
			  dataType: "json",
			  url: ServiceURL + "/WebSvcs/chat/sendMessage/" + GlobalParticipantID,
			  data: JSON.stringify(data),
				success: function( data, textStatus, jQxhr ){
					//console.log(data);
					$( "#PPSWebChat_Message" ).val("");
					setTypingIndicator(false);
					
				},
				error: function( jqXhr, textStatus, errorThrown ){
					alert(errorThrown);
					console.log( errorThrown );
				}
			});
			
		}
		catch(err)
		{
			alert(err);
			console.log(err);		
		}
		
	};

	//Function to set the typing indicator. (This is used so that the agent can see when the user is typing).
	var setTypingIndicator = function (Indicator){
		
		try
		{
			
			var data = { "typingIndicator": Indicator };
		
			$.ajax({
			  type: "POST",
			  dataType: "json",
			  url: ServiceURL + "/WebSvcs/chat/setTypingState/" + GlobalParticipantID,
			  data: JSON.stringify(data),
				success: function( data, textStatus, jQxhr ){
					//console.log(data);	
				},
				error: function( jqXhr, textStatus, errorThrown ){
					alert(errorThrown);
					console.log( errorThrown );
				}
			});
			
		}
		catch(err)
		{
			alert(err);
			console.log(err);			
		}
		

	};
	
	//Function to initiate new Chat with the Service, this gets invoked when user clicks "Start chatting".
	var StartChat = function (){
		
		//Store Name to Session
		StoreToSession("PPSWebChat_ClientName",$( "#PPSWebChat_Name" ).val());
					
		try
		{
				var data = {
				"supportedContentTypes": "text/plain",
				"participant":
				{
					"name": $( "#PPSWebChat_Name" ).val(),
					"credentials": null
				},
				"transcriptRequired": true,
				"emailAddress": $( "#PPSWebChat_Email" ).val(),
				"target": "PPSI Chat Public",
				"targettype": "Workgroup",
				"language": "en-us",
				"customInfo": "Name: " + $( "#PPSWebChat_Name" ).val() + "\r\nContact Number: " + $( "#PPSWebChat_PhoneNumber" ).val() + "\r\nEmail: " + $( "#PPSWebChat_Email" ).val() + "\r\nEnquiry: " + $( "#PPSWebChat_Enquiry" ).val(),
				"attributes" :
				{
				"Name" :  $( "#PPSWebChat_Name" ).val(),
				"Phone_Number" : $( "#PPSWebChat_PhoneNumber" ).val()
				}
			};

			var dataType = "application/json";
			
			
			$.support.cors = true;
			
			$.ajax({
			  type: "POST",
			  dataType: "json",
			  url: ServiceURL + "/websvcs/chat/start",
			  data: JSON.stringify(data),
				success: function( data, textStatus, jQxhr ){
					//console.log(data);
					StoreToSession("GlobalParticipantID",data.chat.participantID);
					GlobalParticipantID = data.chat.participantID;
					pollForNewMessages(GlobalParticipantID);
					
				},
				error: function( jqXhr, textStatus, errorThrown ){
					alert(textStatus);
					console.log( textStatus );
				}
			});
		}
		catch(err)
		{
			alert(err);
			console.log(err);
		}		
					

	};
	
	//Function to exit chat, clear HTML Controls and Session Cache.
	var ExitChat = function(){
		
		try
		{
			var data = null;
		
			$.ajax({
			  type: "POST",
			  dataType: "json",
			  url: ServiceURL + "/WebSvcs/chat/exit/" + GlobalParticipantID,
			  data: JSON.stringify(data),
				success: function( data, textStatus, jQxhr ){
					  //console.log(data);	
					
					  //Hide and Show Pannels
					  $("#PPSWebChat_ChatWithUsPanel").show();
					  $("#PPSWebChat_ChatInfoPanel").hide();
					  $("#PPSWebChat_ChatPanel").hide();
					  
					  //Clear HTML Controls
					  $( "#PPSWebChat_ChatHistory" ).html("");
					  $( "#PPSWebChat_AgentName" ).html("");
					  
					  //Clear Session Cache
					  StoreToSession("GlobalParticipantID", null);		
					  StoreToSession("PPSWebChat_ChatHistory","");
					  StoreToSession("PPSWebChat_ClientName","");
					  StoreToSession("PPSWebChat_AgentName","");
					
				},
				error: function( jqXhr, textStatus, errorThrown ){
					alert(textStatus);
					console.log( textStatus );
				}
			});			
			
		}
		catch(err)
		{
			alert(err);
			console.log(err);		
			
		}
		

		
	};
	
	//Function to store Session Data.
	var StoreToSession = function(Key,Value){
		
		if(Storage)
		{
			window.sessionStorage.setItem(Key,Value);
		}
		else{
			
		}
		
	};
	
	//Function to get Session Data.
	var GetFromSession = function(Key){
		
		if(Storage)
		{
			return window.sessionStorage.getItem(Key);
		}
		else{
			
			return "";
		}
		
	}

	//Function to check Session Data, if data is found HTML Controls is Populated and correct Pannels is shown.
	var CheckSessionData = function(){
		

		
		if(GetFromSession("GlobalParticipantID") != null )
		{
			
			var ParID = GetFromSession("GlobalParticipantID");
			SetGlobalParticipantID(ParID);
			
			var ChatHistory = GetFromSession("PPSWebChat_ChatHistory");
			$("#PPSWebChat_ChatHistory").append(ChatHistory);	
					
			var PPSWebChat_ClientName = GetFromSession("PPSWebChat_ClientName");
			$("#PPSWebChat_ClientName").html(PPSWebChat_ClientName);				

			var PPSWebChat_AgentName = GetFromSession("PPSWebChat_AgentName");
			$("#PPSWebChat_AgentName").html(PPSWebChat_AgentName);
		
			pollForNewMessages(ParID);
			
			
			$("#PPSWebChat_ChatWithUsPanel").hide();
			$("#PPSWebChat_ChatInfoPanel").hide();
			$("#PPSWebChat_ChatPanel").show();
			$('#PPSWebChat_ChatHistory').scrollTop($('#PPSWebChat_ChatHistory').prop("scrollHeight"));
		}
		
		if(GetFromSession("ChatMinimized") != null)
		{
			if(GetFromSession("ChatMinimized") == "Yes")
			{
			  $("#PPSWebChat_ChatWithUsPanel").show();
			  $("#PPSWebChat_ChatInfoPanel").hide();
			  $("#PPSWebChat_ChatPanel").hide();
			  
			}
			else
			{
				if($( "#PPSWebChat_ChatHistory" ).html() == "")
				{
					$("#PPSWebChat_ChatWithUsPanel").hide();
					$("#PPSWebChat_ChatInfoPanel").show();
				}
				else
				{
					$("#PPSWebChat_ChatWithUsPanel").hide();
					$("#PPSWebChat_ChatInfoPanel").hide();
					$("#PPSWebChat_ChatPanel").show();	
				}			
			}
			
		}
		
	}
	
	//Function to Bind HTML Control Events to WebChat Functions above.
	var BindEvents = function(){
		
			$( "#PPSWebChat_ChatWithUsPanel" ).click(function() {
			
			  StoreToSession("ChatMinimized","No");
	  
			  if($( "#PPSWebChat_ChatHistory" ).html() == "")
			  {
				$("#PPSWebChat_ChatWithUsPanel").hide();
				$("#PPSWebChat_ChatInfoPanel").show();
			  }
			  else
			  {
				$("#PPSWebChat_ChatWithUsPanel").hide();
				$("#PPSWebChat_ChatInfoPanel").hide();
				$("#PPSWebChat_ChatPanel").show();	
			  }
			  
			});
			
			$( "#PPSWebChat_LeaveMessagePanel" ).click(function() {
			  $("#PPSWebChat_LeaveMessagePanel").hide();
			  $("#PPSWebChat_ChatWithUsPanel").hide();
			  $("#PPSWebChat_ChatInfoPanel").show();
			  PPSWebChat.Functions.StartChat();	
			});

			$( "#PPSWebChat_StartChatting" ).click(function() {

			  //Clear Validation on Fields
			  $( "#PPSWebChat_Enquiry" ).removeClass( "RequiredField" );
			  $( "#PPSWebChat_Name" ).removeClass( "RequiredField" );
			  $( "#PPSWebChat_PhoneNumber" ).removeClass( "RequiredField" );
			  $( "#PPSWebChat_Email" ).removeClass( "RequiredField" );	
			
			  //Do Validation on Fields
			  if($( "#PPSWebChat_Name" ).val() == "")
			  {
				$( "#PPSWebChat_Name" ).addClass( "RequiredField" );
			  }
			  else if($( "#PPSWebChat_PhoneNumber" ).val() == "")
			  {
				$( "#PPSWebChat_PhoneNumber" ).addClass( "RequiredField" );	  
			  }
			  else if($( "#PPSWebChat_Email" ).val() == "")
			  {
				$( "#PPSWebChat_Email" ).addClass( "RequiredField" );		  
			  }
			  else if($( "#PPSWebChat_Enquiry" ).val() == "")
			  {
				$( "#PPSWebChat_Enquiry" ).addClass( "RequiredField" );
			  }
			  else
			  {
				  //Proceed when Validation passed.
				  $("#PPSWebChat_ChatWithUsPanel").hide();
				  $("#PPSWebChat_ChatInfoPanel").hide();
				  $("#PPSWebChat_ChatPanel").show();
				  $( "#PPSWebChat_ChatHistory" ).html("");
				  $( "#PPSWebChat_AgentName" ).html("");
				  PPSWebChat.Functions.StartChat();	 	  
			  }
			  
		 
			});

			$( "#PPSWebChat_SendMessage" ).click(function() {

				PPSWebChat.Functions.sendWebChatMessage();
			
			});
				
			$( "#PPSWebChat_Message" ).keydown(function(e) {
				
				if(e.which == 13) {
					PPSWebChat.Functions.sendWebChatMessage();	
					e.preventDefault();
					return false;
				}
				else
				{
					PPSWebChat.Functions.setTypingIndicator(true);
				}
				
			});
			
			$( "#PPSWebChat_CloseChat" ).click(function() {

			  $("#PPSWebChat_ChatWithUsPanel").show();
			  $("#PPSWebChat_ChatInfoPanel").hide();
			  $("#PPSWebChat_ChatPanel").hide();	
			  StoreToSession("ChatMinimized","Yes");
			});			
			
			$( "#PPSWebChat_RemoveChat" ).click(function() {

			  PPSWebChat.Functions.ExitChat();	
		  
			});
			
			$( "#PPSWebChat_ChatInfoPanelHeading" ).click(function() {

			  $("#PPSWebChat_ChatWithUsPanel").show();
			  $("#PPSWebChat_ChatInfoPanel").hide();
			  $("#PPSWebChat_ChatPanel").hide();
			  StoreToSession("ChatMinimized","Yes");
			});
		
	}
	
	//Check Offline Hours
	//var CheckIfOffline = function() {
		
	//    //Get UTC Time
    //    var d = new Date();
    //    var n = d.getUTCHours() + 2;
    //    var w = d.getUTCDay();

	//    //if Time is before 8am or after 5pm or on a weekend offline mode will be applied.
    //    if ((w == 0 || w == 6) || (n < 8 || n > 16)) {
    //        $("#PPS_ChatWithUs").text(" Leave a Message");
    //    }
    //    else {
            
    //    }
	//}

    //Determine if holiday or after hours
/*	var CheckIfHolidayOrOffline = function () {
	    var checkCookie = getCookie("PPS_Is_Holiday");
	    if (checkCookie != "undefined" && checkCookie != "") {
	        if (checkCookie == "1") {
	            $("#PPS_ChatWithUs").text(" Leave a Message");
	        } else {
	            CheckIfOffline();
	        }
	    }
	    else {
	        var checkIfHoliday = isHolidayPromise();
	        checkIfHoliday.success(function (data) {
	            var tomorrow = new Date();
	            var tomorrow1 = tomorrow.setDate(tomorrow.getDate() + 1);
	            var tomorrow2 = new Date(tomorrow1);
	            var tomorrow3 = new Date(tomorrow2.toDateString());
	            var tomorrow4 = tomorrow3.toUTCString();

	            if (data.d.results.length > 0) {
	                setCookie("PPS_Is_Holiday", "1", 0.34);
	            } else {
	                setCookie("PPS_Is_Holiday", "0", 0.34);
	            }
	            CheckIfHolidayOrOffline();
	        });
	    }
	}
*/
    //Check Offline Hours
	function CheckIfOffline() {

	    //Get UTC Time
	    var d = new Date();
	    var n = d.getUTCHours() + 2;
	    var w = d.getUTCDay();

	    //if Time is before 8am or after 5pm or on a weekend offline mode will be applied.
	    if ((w == 0 || w == 6) || (n < 8 || n > 16)) {
	        $("#PPS_ChatWithUs").text(" Leave a Message");
	    }
	    else {

	    }
	}

/*	var isHolidayPromise = function () {
	    var getTodayDateUTC = new Date();
	    var getTodayDate = new Date(getTodayDateUTC.toDateString());
	    getTodayDate.setHours(getTodayDateUTC.getHours() + 2);
	    var todayUTC = new Date(getTodayDate.toDateString());
	    var today = new Date(todayUTC);
	    today.setHours(todayUTC.getHours() + 2);
	    var getTodayToISO = today.toISOString().split('.')[0] + "Z";
	    var restUrl = "/_api/Web/Lists/GetByTitle('Public Holidays')/Items?$filter=HolidayDate eq datetime'" + getTodayToISO + "'";
	    return $.ajax({
	        url: _spPageContextInfo.siteAbsoluteUrl + restUrl,
	        type: "GET",
	        headers: {
	            "accept": "application/json;odata=verbose",
	        },
	        success: function (data) {

	        },
	        error: function (error) {
	            console.log(JSON.stringify(error));
	        }
	    });
	}
*/
    //Save a cookie
	function setCookie(cname, cvalue, exdays) {
	    var d = new Date();
	    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	    var expires = "expires=" + d.toUTCString();
	    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

    //Read a cookie
	function getCookie(cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for (var i = 0; i < ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0) == ' ') {
	            c = c.substring(1);
	        }
	        if (c.indexOf(name) == 0) {
	            return c.substring(name.length, c.length);
	        }
	    }
	    return "";
	}
	
	//Return all Functions
	return { CheckIfOffline: CheckIfOffline, BindEvents: BindEvents, CheckSessionData: CheckSessionData, StoreToSession: StoreToSession, GetFromSession: GetFromSession, SetGlobalParticipantID: SetGlobalParticipantID, SetServiceURL: SetServiceURL, pollForNewMessages: pollForNewMessages, sendWebChatMessage: sendWebChatMessage, setTypingIndicator: setTypingIndicator, StartChat: StartChat, ExitChat: ExitChat }
	
}();
