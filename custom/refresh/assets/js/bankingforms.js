//callbackform
$(document).ready(function(){
	function submitBankCallBack( name, surname, emailAddress, contactNumber,idNumber,preferredContactTime,city,province,industry,practicebusiness){

		$.ajax({
			cache: false,
			contentType:'application/json',
			method: 'POST',
			url: 'https://integrations.pps.co.za/api/v1/bankCallback',
			data: JSON.stringify({ 
				emailAddress: emailAddress,
				preferredContactTime: preferredContactTime,
				businessOrPractiseOwner: practicebusiness,
				province: province,
				city: city,
				surname: surname,
				name: name,
				contactNumber: contactNumber,
				industry: industry,
				idNumber: idNumber,
				referringUrl:"string"
	
			 }),
			error: function (data) {
				window.location.href = "/payment-solutions/thank-you";
				console.log(data);
			},
			success: function (data) {
				window.location.href = "/payment-solutions/thank-you";
				console.log(data);	
			
			},
			complete: function(data){
				window.location.href = "/payment-solutions/thank-you";
				console.log(data);
			}
		});
		
	}
	
	function submitInfoDialog(){
	
		 if ($.trim($("#callbackName").val()) && $.trim($('#callbackSurname').val()) && $.trim($('#callbackPassport').val()) && $.trim($('#callbackPhone').val()) && $.trim($('#callbackEmail').val()) && $.trim($('#callbackTime').val()) && $.trim($('#city').val()) && $.trim($('#province').val()) && $.trim($('#industry').val())&& $.trim($('#practicebusiness').val())){
		
			submitBankCallBack( $("#callbackName").val(), $('#callbackSurname').val(), $('#callbackEmail').val(), $('#callbackPhone').val(),$('#callbackPassport').val(),$('#callbackTime').val(),$('#city').val(),$('#province').val(),$('#industry').val(),$('#practicebusiness').val());
		}else{
		 	console.log("form will not be sent");
		 }
		  
	}

	//small form

	function submitInfoCallBack( name, surname, emailAddress, contactNumber,idNumber,preferredContactTime,city,province,industry,practicebusiness){

		$.ajax({
			cache: false,
			contentType:'application/json',
			method: 'POST',
			url: 'https://integrations.pps.co.za/api/v1/bankCallback',
			data: JSON.stringify({ 
				emailAddress: emailAddress,
				preferredContactTime: preferredContactTime,
				businessOrPractiseOwner: practicebusiness,
				province: province,
				city: city,
				surname: surname,
				name: name,
				contactNumber: contactNumber,
				industry: industry,
				idNumber: idNumber,
				referringUrl:"string"
	
			 }),
			error: function (data) {
				debugger;
				window.location.href = "/payment-solutions/thank-you";
				console.log(data);
			},
			success: function (data) {
				debugger;
				window.location.href = "/payment-solutions/thank-you";

				console.log(data);	
			
			},
			complete: function(data){
				debugger;
				window.location.href = "/payment-solutions/thank-you";
				console.log(data);
			}
		});
		
	}
	
	function submitInfo(){	
		if ($.trim($("#callbackName").val()) && $.trim($('#callbackSurname').val()) && $.trim($('#callbackPassport').val()) && $.trim($('#callbackPhone').val()) && $.trim($('#callbackEmail').val()) && $.trim($('#callbackTime').val()) && $.trim($('#city').val()) && $.trim($('#province').val()) && $.trim($('#industry').val())&& $.trim($('#practicebusiness').val())){
		 debugger;
			submitInfoCallBack( $("#_callbackName").val(), $('#_callbackSurname').val(), $('#_callbackEmail').val(), $('#_callbackPhone').val(),$('#_callbackPassport').val(),$('#_callbackTime').val(),$('#_city').val(),$('#_province').val(),$('#_industry').val(),$('#_practicebusiness').val());	  
		}else
		{
			debugger;
			console.log("form will not be sent");
		}
	}
	
	$("#callBackSubmit").on('click',function(){		
		submitInfoDialog();
	});
	 $('#callBackSubmitBottom').on('click',function(){
		submitInfo();	
	});

});