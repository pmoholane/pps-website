/**
 * Name: Cleaner JS
 * Description: Clean Elements, remove unwated wrappers, wrap elements, and typography.
 * Author: Lehlohonolo Motsoeneng
 * Version 1.0
 */

$(document).ready(function () {
    var title = document.createTextNode($('h2 .field--name-field-article-name').text());
    $('h2 .field--name-field-article-name').text('');
    $('h2 .field--name-field-article-name').text(title);

});

function unWrapInner(element){
    //Removes inside Elements and returns the text

    var text=$(''+element).text();
    $(''+element).children().remove();
    //$('h2').text(title);

    return text;
}

function unWrapInnerByClass(element){
    //Removes inside Elements using a class and returns the text

    var text = $('.'+element).text();
    $('.'+element).children().remove();
    return text();
}