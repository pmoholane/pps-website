/**
 * Name: Article Filters JS
 * Description: Filter Articles according to year and month
 * Author: JL Motsoeneng
 * Version 1.0
 */

 var years = []; // Holds all the years foreach post
 var months = []; //Holds all months foreach post
 var uniqueYears = []; // Array of unique years from all posts
 var uniqueMonths = []; // Array of unique months from all posts
 var activeMonths = [];
 var yearID;
 var monthID;
 var activeYear;
 var activeMonth;
 
 
 $(document).ready(function () {
     // Get all years for posts/articles
    getYears();
 
     //Create unique array for years
     uniqueYears.push($.unique(years));
 
 
     displayYears(uniqueYears); // calling display years function
     setDefaults(uniqueYears);
     //getDefaults();
 
     $('.article-year').on('click',function(){
         //get the year on the clicked year filter
         yearID = $(this).attr('id');
 
         activeYear = yearID;
 
         //remove active year class from all years
         $('.article-year').each(function () {
 
             $(this).removeClass('active-year').removeClass('hidden');
 
         });
 
         $(this).addClass('active-year').addClass('hidden');
 
         displayActiveYear(yearID);
         filterArticlesByYear(yearID);
         cleanMonths();
         uniqueMonths = [];
 
         months = getMonths();
 
         uniqueMonths.push($.unique(months));
         activeMonth = uniqueMonths[0][0];
 
         displayMonths(uniqueMonths);
         setActiveMonth(activeMonth);
         filterArticlesByMonth(yearID,activeMonth);
         $('ul.month-list li').hide().slice(0, 5).show();
 
 
     }); //end year select
 
 
 
     var article_link = $('.article-title').find('a').attr('href');
 
     //$('.article-date').attr('href',article_link);
     //$('.views-more-link').attr('href',article_link);
 
 
     //Month Click
     $('.list-inline-item').on('click',function () {
         //alert('Clicked Doc Ready');
 
         $('.list-inline-item').removeClass('active-month');
         $(this).addClass('active-month');
         activeMonth = $(this).find('a').attr('id');
 
 
         $('.release').addClass('hidden'); //Hide all articles first
         $('.release').each(function () {
 
             if ($(this).hasClass(activeYear)==true && $(this).hasClass(activeMonth)==true) {
                 $(this).removeClass('hidden');
             }
             else{
                 $(this).addClass('hidden');
             }
         });
 
 
     });//end month select
     
     $( document ).ready(function() {
        $('.article-title a').attr('target','_parent');
         $('.views-more-link').attr('target','_parent');
    });
 
 
 }); //end Document Ready
 
 
 /**
     * Defined Functions
  * */
 
     function getYears(){
 
             $('.post-year').each(function () {
             years.push($(this).text());
             
 
         });
     }
 
     function setDefaults(uniqueYears){
         // Print the current year on the Dropdown
         $('.filter-button').prepend(uniqueYears[0][0]);
         activeYear = uniqueYears[0][0];
 
         $('.article-year').each(function () {
             if($(this).attr('id')==activeYear){
                 $(this).find('a').addClass('active-year');
                 $(this).addClass('hidden');
             }
         });
 
         filterArticlesByYear(activeYear);
 
         //Get all available months from posts
 
         months = getMonths();
 
         uniqueMonths.push($.unique(months)); // Getting unique Months
         activeMonth = uniqueMonths[0][0];
 
 
         displayMonths(uniqueMonths);
         setActiveMonth(activeMonth);
         filterArticlesByMonth(activeYear,activeMonth);
 
         return activeYear;
 
     } //end set defaults
 
         function getActiveYear(activeYear){
             return activeYear;
         }
 
         function displayActiveYear(year){
 
             $('.filter-button').empty().prepend(year+' <i class="fas fa-caret-down"></i>');
 
         }
 
         function displayActiveMonth(activeMonth){
 
             $('.list-inline-item').removeClass('active-month');
             $('.list-inline-item').each(function () {
 
                 if($(this).find('a').attr('id')==activeMonth){
                     $(this).addClass('active-month');
                 }
             });
             $('.month-filter-button ').empty().prepend(activeMonth);
         }
 
         function getDefaults()
         {
             activeYear = uniqueYears[0][0]; // get the active year
             activeMonth = uniqueMonths[0]; // get the active month
         }
 
         function displayYears(uniqueYears) {
             for (var x = 0; x < uniqueYears[0].length; x++) {
 
                 console.log('Years');
                 console.log(uniqueYears[0][x]);
 
                 $('#filtering-years').append('<li id="' + uniqueYears[0][x] + '" ' +
                     'class="article-year"><a href="javascript:void(0)" style="color:#0b1e39;">' + uniqueYears[0][x] + '</a></li>');
             }
         }
 
         function getMonths(){
             activeMonths = [];
             $('.post-month').each(function () {
 
                 if($(this).parent().parent().hasClass('hidden')==false){
 
                     activeMonths.push($(this).text());
                 }
 
             });
             return activeMonths;
         }
 
         function setActiveMonth(activeMonth){
             //Set Active Month
 
             $('.list-inline-item ').each(function () {
                 if($(this).find('a').attr('id')==activeMonth){
                     $(this).addClass('active-month');
                 }
             });
 
             // Give active class to active month for mobile
             $('.mobile-month-list').each(function () {
                 if ($(this).find('a').attr('id') == activeMonth) {
                     $(this).addClass('active-month');
 
                     //$('filtering-months-mobile').text(id);
                     displayActiveMonth(activeMonth);
                 }
 
             });
         }
 
         function displayMonths(uniqueMonths) {
 
             //append months on a horizontal list
             for (var x = 0; x < uniqueMonths[0].length; x++) {
 
                 var mId = "'"+uniqueMonths[0][x]+"'";
 
                 $('.nav-tabs').append('<li class="list-inline-item">' +
                     '<a data-toggle="tab" href="javascript:void(0)" onclick="filterArticlesByMonth('+ activeYear+',' + mId +')" id="'
                     + uniqueMonths[0][x] + '">' + uniqueMonths[0][x] + '</a></li>');
 
 
             }
 
             $('ul.month-list li').hide().slice(0, 5).show();
 
             //append months on a dropdown for mobile
             for (var x = 0; x < uniqueMonths[0].length; x++) {
                 var mId = "'"+uniqueMonths[0][x]+"'";
                 //if(!$('.release').hasClass('hidden')) {
                 $('.filtering-months-mobile').append('<li class="mobile-month-list">' +
                     '<a  class="month-name" href="javascript:void(0)" onclick="filterArticlesByMonth('+ activeYear+',' + mId +')"' +
                     ' id="' + uniqueMonths[0][x] + '">' + uniqueMonths[0][x] + '</a></li>');
                 //}
 
             }
 
     }
 
 
         function filterArticlesByYear(activeYear){
 
             $('.release').addClass('hidden'); //Hide all articles first
             $('.release').each(function () {
 
                 if ($(this).hasClass(activeYear)==true) {
                     $(this).removeClass('hidden');
                 }
             });
 
             $('.blog').addClass('hidden'); //Hide all articles first
             $('.blog').each(function () {
 
                 if ($(this).hasClass(activeYear)==true) {
                     $(this).removeClass('hidden');
                 }
             });
         }
 
         function filterArticlesByMonth(activeYear,activeMonth){
 
             //alert(activeYear+ activeMonth.toString());
             displayActiveMonth(activeMonth);
 
             $('.release').addClass('hidden'); //Hide all articles first
 
             $('.release').each(function () {
 
                 if ($(this).hasClass(activeYear)==true && $(this).hasClass(activeMonth)==true) {
                     $(this).removeClass('hidden');
                 }
                 else{
                     $(this).addClass('hidden');
                 }
             });
 
 
 
         // For Blogs
         $('.blog').addClass('hidden'); //Hide all articles first
         $('.blog').each(function () {
 
 
             if ($(this).hasClass(activeYear)==true && $(this).hasClass(activeMonth)==true) {
                 $(this).removeClass('hidden');
             }
             else{
                 $(this).addClass('hidden');
             }
         });
 
 
     }
 
     function cleanMonths(){
         $('.list-inline').empty();
         $('.filtering-months-mobile ').empty();
     }
 
 
     /**
      * Actions
      */
 
         // var elementPosition = $('.filter-bar').offset();
         // var page = window.location.pathname;
         // $(window).scroll(function(){
         //     if($(window).scrollTop() > elementPosition.top){
         //         $('.filter-bar').css('position','fixed').css('top','64px').css('right','0px').css('width','100%');
 
         //         //On
         //         //
         //         // scroll Media Queries
 
         //          if($(window).width()<=414){
 
         //                $('.filter-bar').css('top', '0px');
 
         //          }// end mobile max-width:414px
         //         if($(window).width()>=640 && $(window).width()<=765){
 
         //             $('.filter-bar').css('top', '82px').css('background','#041e37');
 
         //         }// end mobile max-width:640px
 
         //         if($(window).width()>=768 && $(window).width()<=1022){
         //             $('.filter-bar').css('top', '82px').css('background','#041e37');
         //         }
 
         //     }
         //     else {
 
         //         $('.filter-bar').css('position','static');
 
 
         //     }
         // }); // End scroll function
 
 
 
 