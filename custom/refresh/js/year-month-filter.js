// duration of scroll animation
var scrollDuration = 300;
// paddles
var leftPaddle = document.getElementsByClassName('left-paddle');
var rightPaddle = document.getElementsByClassName('right-paddle');
// get items dimensions
var itemsLength = $('.sti-item').length;
var itemSize = $('.sti-item').outerWidth(true);
// get some relevant size for the paddle triggering point
var paddleMargin = 20;

// get wrapper width
var getMenuWrapperSize = function() {
	return $('.sti-menu-wrapper').outerWidth();
}
var menuWrapperSize = getMenuWrapperSize();
// the wrapper is responsive
$(window).on('resize', function() {
	menuWrapperSize = getMenuWrapperSize();
});
// size of the visible part of the menu is equal as the wrapper size 
var menuVisibleSize = menuWrapperSize;

// get total width of all menu items
var getMenuSize = function() {
	return itemsLength * itemSize;
};
var menuSize = getMenuSize();
// get how much of menu is invisible
var menuInvisibleSize = menuSize - menuWrapperSize;

// get how much have we scrolled to the left
var getMenuPosition = function() {
	return $('.sti-menu').scrollLeft();
};

// finally, what happens when we are actually scrolling the menu
$('.sti-menu').on('scroll', function() {

	// get how much of menu is invisible
	menuInvisibleSize = menuSize - menuWrapperSize;
	// get how much have we scrolled so far
	var menuPosition = getMenuPosition();

	var menuEndOffset = menuInvisibleSize - paddleMargin;

	// show & hide the paddles 
	// depending on scroll position
	if (menuPosition <= paddleMargin) {
		$(leftPaddle).addClass('hidden');
		$(rightPaddle).removeClass('hidden');
	} else if (menuPosition < menuEndOffset) {
		// show both paddles in the middle
		$(leftPaddle).removeClass('hidden');
		$(rightPaddle).removeClass('hidden');
	} else if (menuPosition >= menuEndOffset) {
		$(leftPaddle).removeClass('hidden');
		$(rightPaddle).addClass('hidden');
}

	// print important values
	// $('#print-wrapper-size span').text(menuWrapperSize);
	// $('#print-menu-size span').text(menuSize);
	// $('#print-menu-invisible-size span').text(menuInvisibleSize);
	// $('#print-menu-position span').text(menuPosition);

});

// scroll to left
$(rightPaddle).on('click', function() {
	$('.sti-menu').animate( { scrollLeft: menuInvisibleSize}, scrollDuration);
});

// scroll to right
$(leftPaddle).on('click', function() {
	$('.sti-menu').animate( { scrollLeft: '0' }, scrollDuration);
});

//
$(document).ready(function(){
	var nonduplicates= [];
	var items= []; 
	var newitems= []; 
	items= $(".sti-item").get();
	 newitems=$(".sti-item").get();
	 $(items).each(
		function(e,etm){
		$(this).remove();
		});

	 $(items).each(
		 function(e,etm){
			  var ims = etm;
			  $(newitems).each(
				  function(i,el)
				  { 
					   if( ims.outerHTML!==el.outerHTML){
						if($.inArray(ims.outerHTML, nonduplicates) == -1 )
						{
							nonduplicates.push(ims.outerHTML);
						} 
					} 
				}
				); 
				
			});
				var month = new Array(); month[0] = "January"; month[1] = "February"; month[2] = "March"; month[3] = "April";
				month[4] = "May";month[5] = "June";month[6] = "July";month[7] = "August";month[8] = "September";month[9] = "October";
				month[10] = "November";month[11] = "December";

				var d = new Date();
				var n = month[d.getMonth()];
				var yyyy = d.getFullYear();
				var cuurentYear= yyyy.toString();
				var dts= '.'+yyyy+'.'+ n;
				console.log(yyyy+' '+ n);
				$(dts);
				$(nonduplicates).each(
					function(ee,eetm){
						var ttttt= $(eetm);
						var classList = ttttt.attr('class').split(/\s+/);;
						debugger;
						var clickent='.blogpage .'+classList[1]+'.'+ classList[2];
						var linkid='.blogpage .'+classList[1]+'.'+ classList[2];
						//for (var i = 0; i < classList.length; i++) {
							if (classList[1] === cuurentYear) { 
								debugger;

								//var myel=$('<a href="#"'>ttttt.innerText+'</a>');
								var myel= $("<a href='#s'>PPS Rewards</a>");
								ttttt.append(myel);
								myel.on("click",function(){	 
									$(linkid).removeClass('blog-hidden');
								});
								$(".sti-menu").append(ttttt);
							}
							else{								
								ttttt.addClass('blog-filter-hidden');
								$(".sti-menu").append(ttttt);
							}
						//}								
					});
});

						
							
						
